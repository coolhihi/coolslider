/*
 * 无缝滚动类
 * 根据在互联网上的较好算法改进所得
 * 可任意转载使用
 * 望保留以下版权
 *
 *    Developed by COoL  |  QQ:176512025  |  Email:176512025@qq.com
 *       _______     _______             _
 *      /  ______\  /  ___  `\    ___   / \
 *     /\  \       /\  \   \  \  / __`\/\  \
 *     \ \  \______\ \  \___\  \/\ \_\ \ \  \_______
 *      \ \________/\ \________/\ \____/\ \________/
 *       \/_______/  \/_______/  \/___/  \/_______/
 *
 */


/*
 * 使用方法:
 * 例:
 * 1.创建对象
 * var testSlider=new CoolSlider("partnerslider",5,true,4000,"partnerprev","partnernext");
 *                    参数意义: 用于滚动的容量ID ,可见数量,自动滚动开关,自动滚动间隔,向左按钮ID,向右按钮ID
 * 2.初始化
 * testSlider.init();
 * 
 * 容量中HTML格式请参考详细示例
 * 
 */
var CoolSlider = function (sliderid, shownum, autoscroll, autoscrollinterval, leftbtnid, rightbtnid) {
    this.sliderId=sliderid;//定义滚动框ID
    this.showNum=shownum;//定义显示多少个li的宽度
    this.autoScroll=autoscroll;//定义是否自动滚动
    this.autoScrollInterval=autoscrollinterval;//定义自动滚动的循环时间
    this.leftBtnId=leftbtnid;//定义向左按钮ID
    this.rightBtnId=rightbtnid;//定义向右按钮ID
    
    this.init = function(){
        /*
         * 初始化对象
         */
        this.scrollUlLeft=0;//默认left为0，即从最左边开始滚动
        this.scrollUlWidth = $('#'+this.sliderId+' .scroll_ul li').outerWidth(true);//单个 li 的宽度
        $('#'+this.sliderId+' .scroll_list').width(this.showNum * this.scrollUlWidth);//根据已定义的li数目设置父容量限宽
        this.prevAllow = true;//用于防止向左按钮连续点击
        this.nextAllow = true;//用于防止向右按钮连续点击
        this.keep = false;//用于在开启自动滚动时保存滚动循环
    
        var _this=this;
        /*
         * 定义左按钮动作
         */
        $('#'+this.leftBtnId).click(function() {
            _this.moveLeft();
        });
        
        /*
         * 定义右按钮动作
         */
        $('#'+this.rightBtnId).click(function() {
            _this.moveRight();
        });
    
        /*
         * 自动滚动
         */
        //检查数量是否超过显示数，超过才开启自动滚动，否则不滚动
        var _totalnum=$("#"+this.sliderId+" li").length;
        if (this.autoScroll && _totalnum>this.showNum) {
            this.keep = setInterval(function() {
                $('#'+_this.rightBtnId).trigger('click');
            }, this.autoScrollInterval);
            $("#"+this.sliderId).mouseover(function(){
                clearInterval(_this.keep);
            });
            $("#"+this.sliderId).mouseleave(function(){
                _this.keep = setInterval(function() {
                    $('#'+_this.rightBtnId).trigger('click');
                }, _this.autoScrollInterval);
            });
        }
    }
    
    /*
     * 左移动作
     */
    this.moveLeft = function(){
        if (this.prevAllow) {
            this.prevAllow = false;
            this.scrollUlLeft = this.scrollUlLeft - this.scrollUlWidth;
            $('#'+this.sliderId+' .scroll_ul').css('left', this.scrollUlLeft);
            //复制最后一个 li 并插入到 ul 的最前面
            $('#'+this.sliderId+' .scroll_ul li:last').clone().prependTo('#'+this.sliderId+' .scroll_ul');
            //删除最后一个 li
            $('#'+this.sliderId+' .scroll_ul li:last').remove();
            var _this=this;
            $('#'+this.sliderId+' .scroll_ul').animate({
                left : _this.scrollUlLeft + _this.scrollUlWidth
                }, 300, function() {
                _this.scrollUlLeft = parseInt($('#'+_this.sliderId+' .scroll_ul').css('left'), 10);
                _this.prevAllow = true;
            });
        }
    };
    
    /*
     * 右移动作
     */
    this.moveRight = function(){
        if (this.nextAllow) {
            this.nextAllow = false;
            var _this=this;
            $('#'+this.sliderId+' .scroll_ul').animate({
                left : _this.scrollUlLeft - _this.scrollUlWidth
                }, 300, function() {
                _this.scrollUlLeft = parseInt($('#'+_this.sliderId+' .scroll_ul').css('left'), 10);
                _this.scrollUlLeft = _this.scrollUlLeft + _this.scrollUlWidth;
                $('#'+_this.sliderId+' .scroll_ul').css('left', _this.scrollUlLeft);
                //复制第一个 li 并插入到 ul 的最后面
                $('#'+_this.sliderId+' .scroll_ul li:first').clone().appendTo('#'+_this.sliderId+' .scroll_ul');
                //删除第一个 li
                $('#'+_this.sliderId+' .scroll_ul li:first').remove();
                _this.nextAllow = true;
            });
        };
    };
};